import { Component, OnInit } from '@angular/core';
import { Client } from '../shared/model/client.model';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../shared/services/client.service';

@Component({
  selector: 'app-client-component',
  templateUrl: './client-component.component.html',
  styleUrls: ['./client-component.component.less']
})
export class ClientComponentComponent implements OnInit {

  displayIt:boolean = false;
  client:Client;
  initClient;
  clients:Array<Client> = [];

  constructor(private formBuilder:FormBuilder,private clientService:ClientService) { 
    this.client = new Client("Zelmad","mohamed","AD205654");
  }

  ngOnInit() {
    this.initFormBuilder();
    /*let date = new Date();
    console.log("day now" + date.getDate());
    console.log("month now" + (date.getMonth()+1));
    let year:number = date.getFullYear();
    let test = year%100;
    console.log("year now" + test);*/

    this.clientService.getAllClients().subscribe(res => this.clients = res);
  }

  initFormBuilder(){
    this.initClient = this.formBuilder.group({
      nomInput : ['',Validators.required],
      prenomInput: ['',[Validators.maxLength(8),Validators.minLength(3)]],
      cinInput:['']
    });

    console.log(this.initClient);
  }

  onSubmit(){
    let client:Client = new Client(this.initClient.value.nomInput,this.initClient.value.prenomInput,this.initClient.value.cinInput);
    console.log(client.nom);
  }
  
  display(){
    this.displayIt = !this.displayIt;
  }

  get nomInput(){
    return this.initClient.nomInput;
  }

  get prenomInput(){
    return this.initClient.prenomInput;
  }

  get cinInput(){
    return this.initClient.cinInput;
  }

}

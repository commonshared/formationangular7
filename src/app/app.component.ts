import { Component, OnInit } from '@angular/core';
import { Demande } from './shared/model/demande.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{
  title = 'fAngular';
  clickedToDisplay:boolean = false;

  demande:Demande;

  constructor(){

  }

  getParam(name:string){
    const results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if(!results){
      return ;
    }
    return results[1] || 0;
  }

  ngOnInit(){
    this.getParam('partner')
  }

  clicked(){
    let param = this.getParam('partner');
    if(param){
      console.log(param);
    }else{
      console.log("Not Defined");
    }
  }
}

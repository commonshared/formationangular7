import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class ClientService{

    constructor(private http:HttpClient){}

    getAllClients():Observable<any>{
        return this.http.get("http://localhost:8990/api/clients");
    }
}
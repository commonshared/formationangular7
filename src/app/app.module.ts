import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientComponentComponent } from './client-component/client-component.component';
import { DemandeComponentComponent } from './demande-component/demande-component.component';
import {HttpClientModule} from '@angular/common/http'
import { ClientService } from './shared/services/client.service';
import { FaticomponentComponent } from './faticomponent/faticomponent.component';
import { ActivatedRoute } from '@angular/router';
@NgModule({
  declarations: [
    AppComponent,
    ClientComponentComponent,
    DemandeComponentComponent,
    FaticomponentComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

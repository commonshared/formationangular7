import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaticomponentComponent } from './faticomponent.component';

describe('FaticomponentComponent', () => {
  let component: FaticomponentComponent;
  let fixture: ComponentFixture<FaticomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaticomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaticomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
